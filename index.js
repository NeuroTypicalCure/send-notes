const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const pretty = require('express-prettify');
const cors = require('cors');

const app = express();

// Mongo
mongoUser = 'send-notes';
mongoPassword = '8y6BjYiEfBbFMLR';
mongoose.connect(''+
'mongodb://send-notes:'+mongoPassword+'@'+mongoUser+'-shard-00-00-uimqq.mongodb.net:27017,'+
'send-notes-shard-00-01-uimqq.mongodb.net:27017,'+
'send-notes-shard-00-02-uimqq.mongodb.net:27017/test?ssl=true&replicaSet=send-notes-shard-0&authSource=admin&retryWrites=true'
,{useNewUrlParser:true});

// Models
const Note = require('./models/note');

// Port (for heroku)
const PORT = process.env.PORT || 3000;

// Middleware
    app.use(cors());
    // bodyparser
    app.use(bodyParser.urlencoded({extended:false}));
    app.use(bodyParser.json());
    // express-prettify
    app.use(pretty({query: 'pretty'}));

// Api routes
app.get('/', (req,res) => {
    res.send(`
        <a href="../notes?pretty">all notes json</a>
        <a href="../nudes">click if you're a hot wamen, or a cold one</a>
    `)
});

app.get('/notes', (req, res) => {
    Note.find()
    .exec()
    .then(notes => {
        console.log(notes);
        res.status(200).json(notes);
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            err: err
        })
    })
});

app.post('/notes', (req, res) => {
    const note = new Note({
        _id: new mongoose.Types.ObjectId(),
        title: req.body.title,
        body: req.body.body
    })
    note.save()
    .then( result =>{
        console.log(result);
    })
    .catch(err => {
        console.log(err)
    });
    res.status(201).json({
        message: 'Handling note POST requests',
        createdNote: note
    });
});

app.get('/notes/:id', (req, res) => {
    const id = req.params.id;
    Note.findById(id)
    .exec()
    .then(note => {
        console.log(note);
        if(note){
            res.status(200).json(note);
        }else{
            res.status(404).json({
                message: 'No valid entry found for' + id
            })
        }
        
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({err: err});
    });
});

app.put('/notes/:id', (req, res) => {
    const id = req.params.id;
    Note.findOneAndUpdate({_id:id},req.body,(err,note) => {
        if(err) {
            console.log(err);
            res.status(500).json({err: err});
        }
        res.status(200).json(note);
    });
});

app.delete('/notes/:id', (req, res) => {
    const id = req.params.id;
    Note.findByIdAndRemove(id)
    .exec()
    .then(() => {
        console.log('Note deleted: '+id)
        res.status(200).json({
            message: 'Note successfully deleted.',
            id: id
        });
    })
    .catch( err => {
        console.log(err);
        res.status(500).json({err:err})
    })
});

app.get('/nudes', (req, res) => {
    res.send('send nudes: <a href="mailto:thomas-vds@live.be">Im such a perv >.> .. b-but im doing it for the memes ok!</a>')
});

app.listen(PORT, () => console.log('listening on port '+PORT));