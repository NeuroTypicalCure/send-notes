send notes
===

Status
---

-[x] Note CRUD    
-[ ] Note object (Nob) CRUD    
-[ ] Security    
Check send-notes-front for more info

Instructions
---

Run on your machine:
```
    node index.js
```
Heroku:
```
    https://send-notes.herokuapp.com/
```
Cors headers:
```css
    Access-Control-Allow-Origin: *
```

Routes:
---

| HTTP Method | Location  | Description       | Type     |
| ----------- | --------- | ----------------- | -------- |
| GET         | /notes    | get all notes     | json     |
| GET         | /notes/id | get note by id    | json     |
| POST        | /notes    | add a note        | json     |
| PUT         | /notes/id | update note by id | json     |
| DELETE      | /notes/id | delete note by id | json     |

Models:
---

Note:
```js
    {
        _id: mongoose.Schema.Types.ObjectId,
        title: String,
        body: String,
    }
```

Disclaimer
---

Ignore my pervy send nudes route    
I mean.. you can send nudes if you want to    
But i just meant it as a meme way of giving you a way to contact me by email..    
.. i swear.. its not because i'm a desperate guy looking for unveiled womenly bodyparts or anything >.>        
